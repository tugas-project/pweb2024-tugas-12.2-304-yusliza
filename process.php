<?php
// Menampilkan kesalahan untuk debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Memeriksa apakah data POST telah diterima
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Mendefinisikan array untuk menyimpan data mahasiswa
    $mahasiswa = [
        "nama" => $_POST['nama'] ?? '',
        "nim" => $_POST['nim'] ?? '',
        "kelas" => $_POST['kelas'] ?? '',
        "semester" => $_POST['semester'] ?? '',
        "jurusan" => $_POST['jurusan'] ?? '',
        "fakultas" => $_POST['fakultas'] ?? '', 
        "dosen_pembimbing" => $_POST['dosen_pembimbing'] ?? '',
        "mata_kuliah" => $_POST['mata_kuliah'] ?? [],
        "nilai" => $_POST['nilai'] ?? [],
        "dosen_pengajar" => $_POST['dosen_pengajar'] ?? []
    ];

    // Fungsi untuk menghitung IPK
    function hitungIPK($nilai) {
        $totalPoin = 0;
        $totalMataKuliah = count($nilai);

        // Menghitung total poin dari semua nilai
        foreach ($nilai as $nilai_mata_kuliah) {
            if ($nilai_mata_kuliah >= 80) $totalPoin += 4.00;
            elseif ($nilai_mata_kuliah >= 76.25) $totalPoin += 3.67;
            elseif ($nilai_mata_kuliah >= 68.75) $totalPoin += 3.33;
            elseif ($nilai_mata_kuliah >= 65) $totalPoin += 3.00;
            elseif ($nilai_mata_kuliah >= 62.50) $totalPoin += 2.67;
            elseif ($nilai_mata_kuliah >= 57.50) $totalPoin += 2.33;
            elseif ($nilai_mata_kuliah >= 55) $totalPoin += 2.00;
            elseif ($nilai_mata_kuliah >= 51.25) $totalPoin += 1.67;
            elseif ($nilai_mata_kuliah >= 43.75) $totalPoin += 1.33;
            elseif ($nilai_mata_kuliah >= 40) $totalPoin += 1.00;
            else $totalPoin += 0;
        }

        // Mengembalikan nilai IPK
        return $totalPoin / $totalMataKuliah;
    }

    // Menghitung IPK
    $ipk = hitungIPK($mahasiswa['nilai']);

    // Menentukan keterangan lulus untuk setiap mata kuliah
    $keteranganLulus = [];
    foreach ($mahasiswa['nilai'] as $nilai) {
        if ($nilai >= 60) {
            $keteranganLulus[] = "LULUS";
        } else {
            $keteranganLulus[] = "GAGAL";
        }
    }

    // Menampilkan data mahasiswa
    echo "<h1>Informasi Mahasiswa</h1>";
    echo "Nama: " . htmlspecialchars($mahasiswa['nama']) . "<br>";
    echo "NIM: " . htmlspecialchars($mahasiswa['nim']) . "<br>";
    echo "Kelas: " . htmlspecialchars($mahasiswa['kelas']) . "<br>";
    echo "Semester: " . htmlspecialchars($mahasiswa['semester']) . "<br>";
    echo "Jurusan: " . htmlspecialchars($mahasiswa['jurusan']) . "<br>";
    echo "Fakultas: " . htmlspecialchars($mahasiswa['fakultas']) . "<br>";
    echo "Dosen Pembimbing: " . htmlspecialchars($mahasiswa['dosen_pembimbing']) . "<br>";
    echo "IPK: " . number_format($ipk, 2) . "<br><br>";

    echo "<h2>Daftar Mata Kuliah</h2>";
    echo "<table border='1'>";
    echo "<tr><th>Mata Kuliah</th><th>Nilai</th><th>Nilai Huruf</th><th>Dosen Pengajar</th><th>Keterangan Lulus</th></tr>";

    // Menampilkan daftar mata kuliah
    for ($i = 0; $i < count($mahasiswa['mata_kuliah']); $i++) {
        $mata_kuliah = htmlspecialchars($mahasiswa['mata_kuliah'][$i]);
        $nilai_mata_kuliah = htmlspecialchars($mahasiswa['nilai'][$i]);
        $dosen_pengajar = htmlspecialchars($mahasiswa['dosen_pengajar'][$i]);
        
        // Menentukan nilai huruf berdasarkan nilai angka
        if ($nilai_mata_kuliah >= 80) $nilai_huruf = "A";
        elseif ($nilai_mata_kuliah >= 76.25) $nilai_huruf = "AB";
        elseif ($nilai_mata_kuliah >= 68.75) $nilai_huruf = "B";
        elseif ($nilai_mata_kuliah >= 65) $nilai_huruf = "BC";
        elseif ($nilai_mata_kuliah >= 62.50) $nilai_huruf = "C";
        elseif ($nilai_mata_kuliah >= 57.50) $nilai_huruf = "D";
        elseif ($nilai_mata_kuliah >= 40) $nilai_huruf = "E";
        else $nilai_huruf = "F";
        
        // Mengambil keterangan lulus dari array
        $keterangan = htmlspecialchars($keteranganLulus[$i]);
        
        echo "<tr><td>$mata_kuliah</td><td>$nilai_mata_kuliah</td><td>$nilai_huruf</td><td>$dosen_pengajar</td><td>$keterangan</td></tr>";
    }
    echo "</table>";
} else {
    echo "Form belum diisi.";
}
?>
