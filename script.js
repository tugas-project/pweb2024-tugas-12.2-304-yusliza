// Fungsi untuk menambahkan input mata kuliah baru
function addCourse() {
    const coursesDiv = document.getElementById('courses');
    const courseCount = document.querySelectorAll('.course').length + 1;

    // Membuat div baru untuk mata kuliah
    const courseDiv = document.createElement('div');
    courseDiv.classList.add('course');

    // Membuat label dan input untuk nama mata kuliah
    const courseLabel = document.createElement('label');
    courseLabel.setAttribute('for', `course${courseCount}`);
    courseLabel.textContent = `Mata Kuliah ${courseCount}:`;
    courseDiv.appendChild(courseLabel);

    const courseInput = document.createElement('input');
    courseInput.setAttribute('type', 'text');
    courseInput.setAttribute('name', 'mata_kuliah[]');
    courseInput.required = true;
    courseDiv.appendChild(courseInput);
    courseDiv.appendChild(document.createElement('br'));

    // Membuat label dan input untuk nilai mata kuliah
    const gradeLabel = document.createElement('label');
    gradeLabel.setAttribute('for', `grade${courseCount}`);
    gradeLabel.textContent = 'Nilai:';
    courseDiv.appendChild(gradeLabel);

    const gradeInput = document.createElement('input');
    gradeInput.setAttribute('type', 'number');
    gradeInput.setAttribute('name', 'nilai[]');
    gradeInput.setAttribute('min', '0');
    gradeInput.setAttribute('max', '100');
    gradeInput.required = true;
    courseDiv.appendChild(gradeInput);
    courseDiv.appendChild(document.createElement('br'));

    // Membuat label dan input untuk dosen pengajar
    const instructorLabel = document.createElement('label');
    instructorLabel.setAttribute('for', `instructor${courseCount}`);
    instructorLabel.textContent = 'Dosen Pengajar:';
    courseDiv.appendChild(instructorLabel);

    const instructorInput = document.createElement('input');
    instructorInput.setAttribute('type', 'text');
    instructorInput.setAttribute('name', 'dosen_pengajar[]');
    instructorInput.required = true;
    courseDiv.appendChild(instructorInput);
    courseDiv.appendChild(document.createElement('br'));

    // Menambahkan div mata kuliah ke dalam form
    coursesDiv.appendChild(courseDiv);
}
